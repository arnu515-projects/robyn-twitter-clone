from robyn import SubRouter
from robyn.templating import JinjaTemplate
from pathlib import Path

router = SubRouter(__file__)
jinja = JinjaTemplate(Path(__file__).parent.parent.resolve() / "views")
