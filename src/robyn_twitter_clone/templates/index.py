from .util import router, jinja


@router.get("/")
def index():
    return jinja.render_template("index.html")
