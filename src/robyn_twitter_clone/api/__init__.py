from robyn import SubRouter

from .healthcheck import router as healthcheck_router

router = SubRouter(__file__, prefix="/api")

router.include_router(healthcheck_router)
