# Twitter clone with Robyn

[Robyn](https://robyn.tech) is a python web framework built with Rust.

This is a twitter clone where I attempt to use robyn.

## Local dev

You need to have [rye](https://rye-up.com) installed.

```
rye sync

rye run dev

# run this in another terminal
rye run tailwind_dev
```
