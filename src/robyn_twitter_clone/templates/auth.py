from .util import router, jinja


@router.get("/login")
def auth_login():
    return jinja.render_template("auth.html", signup=False)


@router.get("/register")
def auth_register():
    return jinja.render_template("auth.html", signup=True)
