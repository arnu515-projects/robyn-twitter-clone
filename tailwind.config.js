/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
		"./src/robyn_twitter_clone/views/**/*.{html,jinja}"
	],
  theme: {
    extend: {
			fontFamily: {
				sans: ["Overpass Variable", "sans-serif"]
			}
		},
  },
  plugins: [
		require("@tailwindcss/container-queries")
	],
}

