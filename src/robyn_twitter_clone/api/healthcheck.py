from robyn import SubRouter
from robyn.robyn import jsonify, Response, Headers

router = SubRouter(__file__, prefix="/api/health")


@router.get("")
def healthcheck():
    return jsonify({"status": "ok"})


@router.get("/")
def healthcheck_r():
    return Response(302, Headers({"Location": "/api/health"}), "")
