from robyn_twitter_clone.app import app
from robyn_twitter_clone.api import router as api_router
from robyn_twitter_clone.templates import router as templates_router


def main() -> int:
    app.include_router(api_router)
    app.include_router(templates_router)

    app.start("0.0.0.0", 5000)
    return 0
