"""
Serves static files
"""

from .util import router
from os import getenv
from pathlib import Path


@router.get("/style.css", const=int(getenv("DEV", "0")) > 0)
def serve_css():
    return {
        "file_path": str(
            Path(__file__).parent.parent.resolve() / "static" / "style.css"
        ),
        "headers": {"Content-Disposition": "attachment", "Content-Type": "text/css"},
    }


# This is the Overpass font.
# For some reason, it only works in firefox if the file name is latin-wght-normal.woff2
@router.get("/latin-wght-normal.woff2", const=int(getenv("DEV", "0")) > 0)
def serve_font():
    return {
        "file_path": str(
            Path(__file__).parent.parent.resolve()
            / "static"
            / "latin-wght-normal.woff2"
        ),
        "headers": {"Content-Disposition": "attachment", "Content-Type": "font/woff2"},
    }
